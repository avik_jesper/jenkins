<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 * .
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$this->load->view('welcome_message');
		echo "welcome_message 199";
		echo "welcome_message 288"
		echo "welcome_message 377";
		echo "welcome_massage 466";
		echo "welcome_message 5";
		echo "welcome_message 6";
		echo "welcome_message 7";
	}

	public function custom()
	{
		$this->load->view('welcome_message');
		echo "adding 1st commit"; 
		echo "adding 2nd csdszdszommit";
		echo "adding 3rd cosaasdmmit";
		echo "adding 4rd commit";
	}
 
	public function custom2()
	{
		$this->load->view('welcome_message');
		echo "adding 1st commit";
		echo "adding 2nd commit";
		echo "adding 3rd commit";
		echo "adding 4rd commit";
		echo "adding 5th commit";
	}
	public function testCanBeCreatedFromValidEmailAddress(): void
    {
        $this->assertInstanceOf(
            Email::class,
            Email::fromString('user@example.com')
        );
    }

    public function testCannotBeCreatedFromInvalidEmailAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Email::fromString('invalid');
    }

    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals(
            'user@example.com',
            Email::fromString('user@example.com')
        );
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	function overview() {
        $data = array();
        $params = $this->_graphAnalyticsParams();

        $data['startTime'] = $params['startTime'];
        $data['endTime'] = $params['endTime'];

        $this->load->model('notifications/m_notifications');

        $this->load->model('notifications/m_daily_stats');
        $totalCount = $this->m_daily_stats->countTotal($params);
       
        $data['totalClick'] = $totalCount['chrome_clicks'] + $totalCount['mozilla_clicks'] + $totalCount['edge_clicks'];
        $data['totalImpression'] = $totalCount['chrome_views'] + $totalCount['mozilla_views'] + $totalCount['edge_views'];
        $data['totalConversion'] = $totalCount['chrome_conversions'] + $totalCount['safari_conversions'] + $totalCount['mozilla_conversions'] + $totalCount['edge_conversions'];

        $data['notificationCount'] = $this->m_daily_stats->fetchDistinctNotifications($params, 1);

        $data['clicksList'] = $this->_loadView('retailer/analytics/home/home_table', $data, TRUE);
        
        if (!empty($_REQUEST['submit'])) {
            $this->_sendResponse($data);
        }
        $response['html'] = $this->_loadView('retailer/analytics/overviewSetCriteria', $data, TRUE);
        $this->_sendResponse($response);
    }
}
